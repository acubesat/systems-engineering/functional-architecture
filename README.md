# Functional Architecture

In this repository the functional flow of the different AcubeSAT system (or operational) modes is detailed. Readers can learn more about what happens in each mode and the interractions between them.

## AcubeSAT Operational Modes

Below you can find the different operational modes for AcubeSAT and the transitions between them.

![](./AcubeSAT_System_Modes.svg)

## Subsystem status during each mode

|                       | Launch/Off    |Commissioning   | Safe           | Nominal| Science|
| ----------------------| ------ | ---------------| -------------- | ------ |--------|
| OBC                   | off    | On             | On             | On     |  On    | 
| EPS                   | off    | On             | On             | On     |  On    |
| COMMS                 | off    | Idle           | Idle           | Idle/on| Idle/On| 
| SU                    | off    | Off            | Idle           | Idle   | On     |
| ADCS                  | off    | On             |Idle/detumbling | Idle/On| Idle/detumbling|
|Thermal                | off    | Idle           | Idle           |  On    | Idle |
|Deployment Mechanism   | off    |Idle(deployment)| Off            |   Off  | off|


## Commissioning mode

![](./Commissioning_Mode.svg)



## Safe mode

![](./Safe_Mode.svg)

## Nominal mode

![](./Nominal_mode.svg)

## Science mode

![](./Science_ModeReworked.svg)

## Boot Sequence

![](./Boot%20Sequence.svg)


## Disclaimer

The AcubeSAT project is carried out with the support of the Education Office of
the [European Space Agency](https://esa.int), under the educational
[Fly Your Satellite!](https://www.esa.int/Education/CubeSats_-_Fly_Your_Satellite/)
programme.

This repository has been authored by university students participating in the
AcubeSAT project. The views expressed herein by the authors can in no way be
taken to reflect the official opinion, or endorsement, of the European Space
Agency.
